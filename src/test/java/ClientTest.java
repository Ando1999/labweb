import Classes.Group;
import Classes.User;
import DB.DB;
import org.junit.Test;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class ClientTest {
    @Test
    public void create() throws SQLException, ClassNotFoundException {
        User user = new User();
        user.setName("user1");
        user.setLogin("user1");
        user.setPassword("password");
        List<Group> groups = new ArrayList<>();
        Group group1 = new Group();
        group1.setGroupName("group1");
        groups.add(group1);
        List<User> users = new ArrayList<>();
        users.add(user);
        group1.setUsers(users);
        user.setGroups(groups);

        DB db = new DB();
        db.insertUser(user);
    }

    @Test
    public void drop() throws SQLException, ClassNotFoundException {

    }
}
