package DB;
import Classes.*;


import javax.persistence.EntityManager;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;
import java.sql.*;
import java.util.List;

public class DB {

    private EntityManager em = Persistence.createEntityManagerFactory("my-persistence-unit").createEntityManager();

    //user
    public List<User> findUsers() throws SQLException {
        Query query = em.createNamedQuery("User.getAll");
        return query.getResultList();
    }

    public User findById(Long userId) throws SQLException {
        return em.find(User.class, userId);
    }

    public List<User> findUserByLogin(String login) throws SQLException {
        TypedQuery<User> query = em.createNamedQuery("User.getByLogin", User.class);
        query.setParameter("login", login);
        return query.getResultList();
    }

    public void insertUser(User user) throws SQLException {
        em.getTransaction().begin();
        em.persist(user);
        em.getTransaction().commit();
    }

    public Long updateUser(User user) throws SQLException {
        em.getTransaction().begin();
        User userNew = em.merge(user);
        em.getTransaction().commit();
        return userNew.getId();
    }



    //group
    public  Group findGroupById(Long groupId) throws SQLException, ClassNotFoundException {
        return em.find(Group.class, groupId);
    }

    public List<Group> findGroups() throws SQLException {
        Query query = em.createNamedQuery("Group.getAll");
        return query.getResultList();
    }

    public void insertGroup(Group group) throws SQLException {
        em.getTransaction().begin();
        em.persist(group);
        em.getTransaction().commit();
    }

    public void updateGroup(Group group) throws SQLException {
        em.getTransaction().begin();
        em.merge(group);
        em.getTransaction().commit();
    }

}
