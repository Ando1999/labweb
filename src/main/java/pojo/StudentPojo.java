package pojo;

import lombok.Data;

import javax.xml.bind.annotation.XmlRootElement;
import java.sql.Date;

@Data
@XmlRootElement(name="student")
public class StudentPojo {

    private Long id;
    private String FIO;
    private Long Nzach;
    private String Tema;
    private double average;
    private Long groupId;

    public StudentPojo() {
    }
}
