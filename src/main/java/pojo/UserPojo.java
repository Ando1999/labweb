package pojo;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;


@Data
@EqualsAndHashCode(exclude = {"groups"})
@XmlRootElement(name="user")
@XmlAccessorType(XmlAccessType.FIELD)
public class UserPojo {

    private Long id;
    private String name;
    private String login;

    @XmlTransient
    private String password;

    @XmlElementWrapper(name = "groups")
    @XmlElement(name = "group")
    private List<GroupPojo> groups = new ArrayList<>();

    public UserPojo(){
    }

    public void addGroup(GroupPojo group){
        groups.add(group);
    }
}
