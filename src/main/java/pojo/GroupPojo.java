package pojo;

import Classes.Student;
import lombok.Data;
import lombok.ToString;

import javax.xml.bind.annotation.*;
import java.util.ArrayList;
import java.util.List;

@Data
@ToString(exclude = "students")
@XmlRootElement(name = "group")
@XmlAccessorType(XmlAccessType.FIELD)
public class GroupPojo {

    private Long id;
    private String groupName;
    @XmlElementWrapper(name = "students")
    @XmlElement(name = "student")
    private List<StudentPojo> students = new ArrayList<>();

    public GroupPojo() {
    }

    public void addStudent(StudentPojo student){
        students.add(student);
    }



}

