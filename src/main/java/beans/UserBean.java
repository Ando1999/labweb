package beans;

import Classes.Group;
import Classes.Student;
import Classes.User;
import pojo.GroupPojo;
import pojo.StudentPojo;
import pojo.UserPojo;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.jws.soap.SOAPBinding;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.sql.SQLException;

@ManagedBean(name = "user", eager = true)
@SessionScoped
public class UserBean {

    private UserEjb userEjb = new UserEjb();
    private User user = new User();

    public String getLogin() {
        return user.getLogin();
    }

    public void setLogin(String login) {
        this.user.setLogin(login);
    }

    public String getPassword() {
        return user.getPassword();
    }

    public void setPassword(String password) {
        this.user.setPassword(password);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String validateUser() throws SQLException, ClassNotFoundException {
        if(userEjb.validateUser(user) != null){
            user = userEjb.validateUser(user);
            return "result";
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Неверные данные"));
            return "login";
        }
    }


    public String logout() {
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.invalidate();
        return "login";
    }

    public UserPojo fillUser(){
        UserPojo userPojo = new UserPojo();
        userPojo.setId(user.getId());
        userPojo.setLogin(user.getLogin());
        userPojo.setPassword(user.getPassword());
        userPojo.setName(user.getName());
        for(Group gr: user.getGroups()){
            GroupPojo groupPojo = new GroupPojo();
            groupPojo.setId(gr.getId());
            groupPojo.setGroupName(gr.getGroupName());
            for(Student stu: gr.getStudents()){
                StudentPojo studentPojo = new StudentPojo();
                studentPojo.setId(stu.getId());
                studentPojo.setFIO(stu.getFIO());
                studentPojo.setTema(stu.getTema());
                studentPojo.setNzach(stu.getNzach());
                studentPojo.setAverage(stu.getAverage());
                studentPojo.setGroupId(stu.getGroup().getId());
                groupPojo.addStudent(studentPojo);
            }
            userPojo.addGroup(groupPojo);
        }
        return userPojo;
    }

    public void convert() throws JAXBException, IOException {
        JAXBContext context = JAXBContext.newInstance(UserPojo.class);
        Marshaller m = context.createMarshaller();
        m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
        ByteArrayOutputStream bos = new ByteArrayOutputStream();

        FacesContext ctx = FacesContext.getCurrentInstance();
        HttpServletResponse response = (HttpServletResponse) ctx.getExternalContext().getResponse();

        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment;filename=file.xml");

        m.marshal(fillUser(), bos);

        OutputStream os = response.getOutputStream();
        bos.writeTo(os);
        os.flush();
        bos.close();
        ctx.getResponseComplete();
    }

    public String moveToInsertGroup(){
        return "insgroup";
    }

    public String moveToInsertStudent(){
        return "insstudent";
    }

}
