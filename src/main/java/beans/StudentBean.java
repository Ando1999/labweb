package beans;

import Classes.Group;
import Classes.Student;
import Classes.User;
import DB.DB;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "student", eager = true)
public class StudentBean {
    @ManagedProperty(value="#{user}")
    private UserBean userBean;
    private Student student = new Student();
    private DB db = new DB();

    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public UserBean getUserBean() {
        return userBean;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public void setGroupId(Long groupId){
        HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
        session.setAttribute("group_id", groupId);
    }

    public String insert() throws SQLException, ClassNotFoundException {
        if (student.getAverage() >= 2.0 && student.getAverage() <= 5.0) {
            HttpSession session = (HttpSession) FacesContext.getCurrentInstance().getExternalContext().getSession(false);
            Long groupId = (Long) session.getAttribute("group_id");
            student.setGroup(db.findGroupById(groupId));

            User user = userBean.getUser();

            List<Group> groups = user.getGroups();
            for (Group gr : groups) {
                if (gr.getId().equals(groupId)) {
                    gr.addStudent(student);
                    db.updateGroup(gr);
                }
            }

            userBean.setUser(db.findById(user.getId()));
            return "result";
        } else {
            FacesContext.getCurrentInstance().addMessage(null,
                    new FacesMessage("Неверные данные"));
            return "insstudent";
        }
    }
}
