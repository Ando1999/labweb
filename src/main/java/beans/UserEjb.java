package beans;

import Classes.User;
import DB.DB;

import javax.ejb.Stateless;
import java.sql.SQLException;

@Stateless
public class UserEjb {
    private DB db = new DB();

    public User validateUser(User user) throws SQLException {
        User userFull = db.findUserByLogin(user.getLogin()).get(0);
        if (userFull.getPassword().equals(user.getPassword())) {
            return userFull;
        }
        return null;
    }

}
