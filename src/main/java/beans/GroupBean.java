package beans;

import Classes.Group;
import Classes.User;
import DB.DB;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

@ManagedBean(name = "group", eager = true)
public class GroupBean {
    @ManagedProperty(value="#{user}")
    private UserBean userBean;
    private Group group = new Group();
    private DB db = new DB();

    public Group getGroup() {
        return group;
    }

    public void setGroup(Group group) {
        this.group = group;
    }

    public void setUserBean(UserBean userBean) {
        this.userBean = userBean;
    }

    public String insert() throws SQLException, ClassNotFoundException {
        User user = userBean.getUser();
        group.addUser(user);
        user.addGroup(group);
        Long userId = db.updateUser(user);

        userBean.setUser(db.findById(userId));
        return "result";
    }
}
