package Classes;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;


@Entity(name = "user")
@Table(name = "customer")
@Data
@EqualsAndHashCode(exclude = {"groups"})
@NamedQueries({
        @NamedQuery(name = "User.getByLogin", query = "from user c where login = :login"),
        @NamedQuery(name = "User.getAll", query = "from user")
})
public class User {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String login;

    private String password;

    @ManyToMany(mappedBy = "users", cascade = {CascadeType.ALL})
    private List<Group> groups = new ArrayList<>();


    public User(){}

    public void addGroup(Group group){
        groups.add(group);
    }


}
