package Classes;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.Serializable;
import java.text.ParseException;

@Data
@ToString(exclude = {"group"})
@Entity(name = "student")
@Table
@NamedQueries({
        @NamedQuery(name = "Student.getAll", query ="from student "),
        @NamedQuery(name = "Student.getByArticleId", query ="from student where student_id = :student_id")
})
public class Student {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String FIO;
    private Long Nzach;
    private String Tema;
    private double average;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "group_id", nullable=false)
    private Group group;

    public Student(){

    }

}
