package Classes;

import lombok.Data;
import lombok.ToString;

import javax.persistence.*;
import java.io.*;
import java.util.*;

@Data
@Entity(name = "group")
@Table(name="groups")
@ToString(exclude = {"users", "students"})
@NamedQueries({
        @NamedQuery(name = "Article.getAll", query = "from group")
})
@NamedNativeQueries({
        @NamedNativeQuery(name = "ArticleList.add",
                query = "INSERT INTO article_list(article_id, client_id) values(?, ?)")
})
public class Group implements Serializable{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Long id;
    private String groupName;

    @OneToMany(mappedBy = "group", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private List<Student> students = new ArrayList<>();


    @ManyToMany(cascade = { CascadeType.ALL })
    @JoinTable(
            name = "group_list",
            joinColumns = { @JoinColumn(name = "group_id", referencedColumnName = "id", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "user_id", referencedColumnName = "id", nullable = false, updatable = false) }
    )
    private List<User> users = new ArrayList<>();

    public Group(){
    }

    public void addUser(User user){
        users.add(user);
    }

    public void addStudent(Student student){
        students.add(student);
    }

}
